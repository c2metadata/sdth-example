"Example_Python_code.py" -- Python data transformation script used in this example

"Example_SDTL.json"  -- SDTL JSON file for the example Python code

"jsonLD_to_ttl.py" -- Python program that reads JSON-LD  writes Turtle ttl

"SDTH_Example.json"  -- SDTH for example in JSON-LD

"SDTH_Example.ttl" -- SDTH for example in ttl  -- derived from JSON-LD version

"SPARQL_queries_on_ttl.py" -- Python code that runs SPARQL queries on ttl

"SDTH_example_SPARQL_results.txt"  -- Results of SPARQL queries