##############################################################
####   Read ttl file and run SPARQL queries
####   Prompts for working directory and ttl file
####   Queries are in this program
##############################################################
import pandas as pd
import io
import os
import json
import csv
import tkinter as tk
from tkinter import filedialog
from rdflib import Graph, plugin
from rdflib.serializer import Serializer
from rdflib import Namespace
import rdflib

root = tk.Tk()
root.withdraw()

########  prompt for working directory #########
qpath = filedialog.askdirectory(title="Select output directory")

## -- Prompt for ttl file ---
file_path = filedialog.askopenfilename(title="Select ttl file")
print(file_path)

fld = Graph()


## -- Read ttl file ---
##with open(file_path) as f:
fld.parse(file_path)

#### -- extract file name from .ttl file
fn1 = file_path[ len(qpath) + 1:]
fn2 = fn1[:-4]

##print("**** graph *****")
print("Number of triples: ", len(fld) , "\n")


###### Open query output file ##########
with open(f"{qpath}/{fn2}_SPARQL_results.txt", 'w') as qoutput:

    ####################
    print("\n\n********* Object Names     *******************")
    qoutput.write("\n\n*********  Object Names    *******************\n")
    query_txt = """
    PREFIX sdth: <http://DDI/SDTH/>
    PREFIX sdtest: <http://test/#>	

    SELECT distinct  ?s ?sname
    WHERE {
 		?s  sdth:hasName ?sname .
       }"""

    qres = fld.query(query_txt)
    for row in qres:
        print(f"{row.s} has name {row.sname} " )
        qoutput.write(f"{row.s} has name {row.sname} \n")
 
    ##########    


    ####################
    print("\n\n*********  wasDerivedFrom query for variable name HHcateg *******************")
    qoutput.write("\n\n*********  wasDerivedFrom query for variable name HHcateg   *******************\n")
    query_txt = """
    PREFIX sdth: <http://DDI/SDTH/>
    PREFIX sdtest: <http://test/#>	

    SELECT distinct  ?sname ?oname
    WHERE {
        ?s  sdth:wasDerivedFrom+ ?o .
		?s  sdth:hasName ?sname .
		?o  sdth:hasName ?oname .
        FILTER (?sname = "HHcateg")
        }"""

    qres = fld.query(query_txt)
    for row in qres:
        print(f"{row.sname} is derived from {row.oname} " )
        qoutput.write(f"{row.sname} is derived from {row.oname} \n")

    ##########    

    ####################
    print("\n\n*********  Has an impact on query for variable name PPHHSIZE  *******************")
    qoutput.write("\n\n*********  Has an impact on query for variable name PPHHSIZE  *******************\n")
    query_txt = """
    PREFIX sdth: <http://DDI/SDTH/>
    PREFIX sdtest: <http://test/#>	

    SELECT distinct  ?sname ?oname
    WHERE {
        ?s  sdth:wasDerivedFrom+ ?o .
		?s  sdth:hasName ?sname .
		?o  sdth:hasName ?oname .
        FILTER (?oname = "PPHHSIZE")
        }"""

    qres = fld.query(query_txt)
    for row in qres:
        print(f"{row.oname} has an impact on {row.sname} " )
        qoutput.write(f"{row.oname} has an impact on {row.sname} \n" )

    ##########    

    print("\n\n*********  ProgramSteps affecting variable name HHcateg  *******************")
    qoutput.write("\n\n*********  ProgramSteps affecting variable name HHcateg   *******************\n")
    query_txt = """
    PREFIX sdth: <http://DDI/SDTH/>
    PREFIX sdtest: <http://test/#>	

    SELECT ?sname ?oname  ?pscode
    WHERE {
        ?s  sdth:wasDerivedFrom+ ?o .
		?s  sdth:hasName ?sname .
		?o  sdth:hasName ?oname .
        ?pstep sdth:assignsVariable ?o.
        ?pstep sdth:hasSourceCode ?pscode.
        FILTER (?sname = "HHcateg")
     }"""

    qres = fld.query(query_txt)
    for row in qres:
        print(f"{row.sname} is affected by {row.oname} in command [{row.pscode}]  " )
        qoutput.write(f"{row.sname} is affected by {row.oname} in command [{row.pscode}]  \n" )
    ##########


    ##########    

    print("\n\n*********  Variables indirectly affected by ProgramStep ProgStep004 *******************")
    qoutput.write("\n\n*********  Variables indirectly affected by ProgramStep ProgStep004*******************\n")
    query_txt = """
    PREFIX sdth: <http://DDI/SDTH/>
    PREFIX sdtest: <http://test/#>	

    SELECT distinct  ?sname ?oname ?pscode 
    WHERE {
        ?s  sdth:wasDerivedFrom+ ?o .
		?s  sdth:hasName ?sname .
		?o  sdth:hasName ?oname .
		
        {SELECT ?o  ?pscode
        WHERE {
            ?pstep sdth:assignsVariable ?o.
            ?pstep sdth:hasSourceCode ?pscode.
            FILTER (?pstep = sdtest:ProgStep004 )
         }
         }

    }"""

    qres = fld.query(query_txt)
    for row in qres:
        print(f"{row.sname} is affected by {row.oname}, which is modified in command [{row.pscode}]" )
        qoutput.write(f"{row.sname} is affected by {row.oname}, which is modified in command [{row.pscode}]\n" )
    ##########    
####################
    print("\n\n********* Find a File containing variable PPHHSIZE that is Loaded in the Program      *******************")
    qoutput.write("\n\n********* Find a File containing variable PPHHSIZE that is Loaded in the Program      *******************\n")
    query_txt = """
    PREFIX sdth: <http://DDI/SDTH/>
    PREFIX sdtest: <http://test/#>	

    SELECT distinct  ?sname ?pscode 
    WHERE {
                ?s  sdth:hasVarInstance ?v .
                ?v  sdth:hasName 'PPHHSIZE' .
                ?ps  sdth:loadsFile ?s .
                ?ps sdth:hasSourceCode ?pscode .
                ?s sdth:hasName ?sname .
       }"""

    qres = fld.query(query_txt)
    for row in qres:
        print(f"Command [{row.pscode}] loads {row.sname} containing variable 'PPHHSIZE' " )
        qoutput.write(f"Command [{row.pscode}] loads {row.sname} containing variable 'PPHHSIZE' \n" )
##########    

####################
    print("\n\n*********  elaborationOf query for variable name HHcateg *******************")
    qoutput.write("\n\n*********  elaborationOf query for variable name HHcateg   *******************\n")
    query_txt = """
    PREFIX sdth: <http://DDI/SDTH/>
    PREFIX sdtest: <http://test/#>	

    SELECT distinct ?s ?o ?sname ?oname ?pscode
    WHERE {
        ?s  sdth:elaborationOf+ ?o .
		?s  sdth:hasName ?sname .
		?o  sdth:hasName ?oname .
	?pstep sdth:usesVariable ?o.
            ?pstep sdth:hasSDTL ?pscode.

        FILTER (?sname = "HHcateg")
        }"""

    qres = fld.query(query_txt)
    for row in qres:
        print(f"{row.s} named {row.sname} is elaboration of {row.o} named  {row.oname} in command {row.pscode} " )
        qoutput.write(f"{row.s} named {row.sname} was Based On {row.o} named {row.oname}  in command {row.pscode} \n")

    ##########   
    ####################
    print("\n\n*********  usesVariable query for variable name PPHHSIZE *******************")
    qoutput.write("\n\n*********  usesVariable query for variable name PPHHSIZE   *******************\n")
    query_txt = """
    PREFIX sdth: <http://DDI/SDTH/>
    PREFIX sdtest: <http://test/#>	

    SELECT distinct  ?psource ?oname
    WHERE {
        ?pstep  sdth:usesVariable+ ?o .
		?pstep  sdth:hasSourceCode ?psource .
		?o  sdth:hasName ?oname .
        FILTER (?oname = "PPHHSIZE")
        }"""

    qres = fld.query(query_txt)
    for row in qres:
        print(f"Command [{row.psource}] is affected by {row.oname} " )
        qoutput.write(f"Command [{row.psource}] is affected by {row.oname} \n")

    ##########
        
    ####################################################    
    print("\n\n*********  usesVariable query for indirect effects of variable name PPHHSIZE   *******************")
    qoutput.write("\n\n*********  usesVariable query for indirect effects of variable name PPHHSIZE  *******************\n")
    query_txt = """
    PREFIX sdth: <http://DDI/SDTH/>
    PREFIX sdtest: <http://test/#>	

    SELECT distinct  ?psource ?sname ?oname
    WHERE {
        ?pstep  sdth:usesVariable+ ?s .
		?pstep  sdth:hasSourceCode ?psource .
	  
		{SELECT distinct  ?s  ?sname ?oname
		WHERE {
			?s  sdth:wasDerivedFrom+ ?o .
			?s  sdth:hasName ?sname .
			?o  sdth:hasName ?oname .
			FILTER (?oname = "PPHHSIZE" )  
			}
			}
		}"""

    qres = fld.query(query_txt)
    for row in qres:
        print(f"Command [{row.psource}] is affected by {row.sname} which is affected by {row.oname} " )
        qoutput.write(f"Command [{row.psource}] is affected by {row.sname} which is affected by {row.oname} \n" )
    ##########


    ####################
    print("\n\n*********  usesVariable query DIRECT AND INDIRECT for variable name PPHHSIZE *******************")
    qoutput.write("\n\n*********  usesVariable query DIRECT AND INDIRECT for variable name PPHHSIZE   *******************\n")
    query_txt = """
    PREFIX sdth: <http://DDI/SDTH/>
    PREFIX sdtest: <http://test/#>	

    SELECT distinct  ?psource ?oname

		WHERE {

		{
			?pstep  sdth:usesVariable+ ?o .
			?pstep  sdth:hasSourceCode ?psource .
			?o  sdth:hasName ?oname .
			FILTER (?oname = "PPHHSIZE")
			}
				
		UNION

		{
			?pstep  sdth:usesVariable+ ?s2 .
			?pstep  sdth:hasSourceCode ?psource .
		  
			{SELECT distinct  ?s2   ?oname
			WHERE {
				?s2  sdth:wasDerivedFrom+ ?o2 .
				?s2  sdth:hasName ?oname .
				?o2  sdth:hasName ?o2name .
				FILTER (?o2name = "PPHHSIZE" )  
				}
				}
			}
		}
		"""

    qres = fld.query(query_txt)
    for row in qres:
        print(f"Command [{row.psource}] is affected by {row.oname} " )
        qoutput.write(f"Command [{row.psource}] is affected by {row.oname} \n" )
    ##########

